// ==UserScript==
// @name        Invidious No JS
// @namespace   *://invidious.com/*
// @include     *://iteroni.com/*
// @version     1
// ==/UserScript==

// Description : when the site is an invidious domain (in my case i set it to iteroni), automatically add the nojs=1 URL parameter to the site.
// This means no javascript is properly supported, you will see comments etc even without js.

var TargetURL = new URL(window.location.href);

if ( (TargetURL.searchParams.get("nojs")) != 1 )
{
  TargetURL.searchParams.append("nojs", 1)

  window.location.replace(TargetURL);
}
